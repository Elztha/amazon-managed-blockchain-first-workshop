# Amazon Managed Blockchain Basic Introduction Workshop

## Scenario

<div align="center">
<img src="images/hyperledger_logo_new.png" width="50%" style="margin-top: 10px;"/><img src="images/aws-mb-logo.png" width="30%" style="margin-top: 10px;margin-left: 30px;"/>
</div>

[Amazon Managed Blockchain](https://aws.amazon.com/managed-blockchain/) is a fully managed service that makes it easy to create and manage scalable Blockchain Networks using the popular open source frameworks [Hyperledger Fabric](https://www.hyperledger.org/projects/fabric) and [Ethereum](https://www.ethereum.org/)*.

This workshop will walk you through on creating a Hyperledger Fabric blockchain network using Amazon Managed Blockchain. 
	
## Overview

<img src="images/System Structure.png" width="80%" style="margin-top: 10px;margin-left: 40px;"/>
	
We are making a 1 Node 1 Member Fabric Network Infrastructure with Amazon Managed Blockchain and then connecting to it through VPC Private Link, given by from the service.

## Prerequisites
> Prepare an AWS account. 

> Make sure your are in US East (N. Virginia), which short name is us-east-1.


### Clone This Lab

<img src="images/screenshot.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>

Download this Project as .zip and extract it as you will need the files in **template** folder

---


## Step By Step

### Create Your Network

Make sure you are in the correct AWS region **(i.e. us-east-1, also known as N. Virginia)** and follow the steps below:

1. On the **Services** menu, select **Amazon Managed Blockchain**.
2. Click Create a Network
3. Make sure **Hyperledger Fabric 1.2** is selected

	<img src="images/tutorial-1.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>

4. Enter the **Network name** and **Description**, and click **Next**. (DO NOT use spaces or special characters in the network name), the lab will be using:
    - Network name: `AWS-MB-Tutorial`
    - Description: `Blockchain Network Test for Workshop`

 	<img src="images/tutorial-2.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>

5. Enter the first member name (e.g. this could be the name of the organisation you belong to) and an optional description, the lab will be using:

	- Member name: `firstmember-<name>`
	- Description: `The first member`

	<img src="images/tutorial-3.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>
	
6. Enter an admin username and password, and **note this down**. *You will need it later.* Click **Next**

	<img src="images/tutorial-4.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>
	
7. Click **Create network and member** and wait until the status of both your network and member become **Available**.

	<img src="images/tutorial-5.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>


____

### Creating your Infrastructure (CloudFormation)

1. On the **Services** menu, select **CloudFormation**.

2. On the left navigation panel, select **Stacks**, then select **Create Stack**.

3. Select **Template is ready** and **Upload a template file**

	<img src="images/tutorial-6.png" width="70%" style="margin-top: 10px;margin-left: 30px;"/>


4. Choose the [fabric-client-node.yaml](materials/fabric-client-node.yaml) code, attached to this project for the template source and click **Next**

5. On the next window, enter name of your cloudformation stack as **Name : `MB-Fabric-Workshop`** and insert your **Managed Blockchain VPC Endpoint**, and specify your **KeyName : `MB-Workshop Key`** file attached to this project file for the instance which will connect to the blockchain network and click **Next**

	<img src="images/tutorial-7.png" width="50%" style="margin-top: 10px;margin-left: 30px;"/><img src="images/tutorial-8.png" width="50%" style="margin-top: 10px;margin-left: 30px;"/>
	
	>Copy the VPC Endpoint on your Managed Blockchain Network After your network is **Available**, **YOU DON'T NEED TO CREATE VPC ENDPOINT**, as it has been created automatically for you	
	
	
6. On the next window, enter the IAM Roles that Cloudformation would use to create your stack in this tutorial I am using `Cloudformation-admin`, and click **Next**

	
	<img src="images/tutorial-10.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>
	
7. After that you will be asked to acknowledge that CloudFormation will create IAM Resources **put a tick** on the "I acknowledge ... IAM resouces" and click on **Create stack**

	<img src="images/tutorial-11.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>
	
### Creating Peer Node

8. While the stack is creating go back to AWS Services and select **Amazon Managed Blockchain**, Select **AWS-MB-Tutorial**

9. Select the tab **Members** and select the first member that you have created and click **Create Peer Nodes**

10. Pick *bc.t3.small* type and *us-east-A* Availability Zone and click **Create Peer Node**
	<img src="images/tutorial-9.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>
_______

### Creating Fabric Node Client


1. Modify [exports-template.sh](templates/exports-template.sh) file attached on this workshop in **template** folder into your instance or just create a new one from it.

	 Remember to replace the :

	- NETWORKNAME = `<your network name>`
	- MEMBERNAME = `<your member name when creating network>`
	- ADMINUSER = `<your admin username>`
	- ADMINPWD = `<your admin password>`
	- NETWORKID = `<your network ID from AWS console>`
	- MEMBERID = `<your member ID from AWS console>`

	<img src="images/tutorial-14.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>

2. SSH into the EC2 instance created from our Cloudformation stack earlier (Make sure you are logged-in as **ec2-user**), and run **sudo yum update**

	<img src="images/tutorial-12.png" width="80%" style="margin-top: 10px;margin-left: 30px;"/>

	> commands : ssh -i "< yourkeyfile.pem >" ec2-user@ec2-xx-xx-xx-xxx.compute-1.amazonaws.com
	
	>*your key file : the keyname specified on step 5*
	
3. Copy and paste your modified **exports-template.sh** file into your EC2 Instance 

	> vi exports-template.sh (Create new exports-template.sh with vim editor)
	
	> (Paste the file with cmd+v on Mac and right click on windows)
	
	> :wq (to write file and quit vim)

4. After you copied our [exports-template.sh](templates/exports-template.sh) ,source the command

	> [ec2-user@ip-10-0-176-173 ~]$ **source exports-template.sh**
	
	If there is no error you can see your created peer informations :
	<img src="images/tutorial-15.png" width="100%" style="margin-top: 10px;"/>
	
	Sourcing the file will do two things:

	- export the necessary Environmental variables 
	- create another file which contains the export values you need to use when working with a Fabric peer node. This can be found in the file: **~/peer-exports.sh**. You will see how to use this in a later step.

5. Check if the peer-export.sh file exists and that it contains a number of export keys with values:

	>[ec2-user@ip-10-0-176-173 ~]$ **cat ~/peer-exports.sh**
	
	If it exists, then source it by typing : 
	
	>[ec2-user@ip-10-0-176-173 ~]$ **source ~/peer-exports.sh**

6. Get the latest version of the Managed Blockchain PEM file. This will overwrite the existing file in the home directory with the latest version of this file:
	>[ec2-user@ip-10-0-176-173 ~]$ **aws s3 cp s3://us-east-1.managedblockchain/etc/managedblockchain-tls-chain.pem  /home/ec2-user/managedblockchain-tls-chain.pem**
	
7. Enroll an admin identity with the Fabric CA (certificate authority). We will use this identity to administer the Fabric network and perform tasks such as creating channels and instantiating chaincode.

	>[ec2-user@ip-10-0-176-173 ~]$ **export PATH=$PATH:/home/ec2-user/go/src/github.com/hyperledger/fabric-ca/bin**
	
	>[ec2-user@ip-10-0-176-173 ~]$ **fabric-ca-client enroll -u https://$ADMINUSER:$ADMINPWD@$CASERVICEENDPOINT --tls.certfiles /home/ec2-user/managedblockchain-tls-chain.pem -M /home/ec2-user/admin-msp**
	
	<img src="images/tutorial-16.png" width="100%" style="margin-top: 10px;"/>

8. Specify the certificate you just created as an admin certificate

	>[ec2-user@ip-10-0-176-173 ~]$ **mkdir -p /home/ec2-user/admin-msp/admincerts**
	
	>[ec2-user@ip-10-0-176-173 ~]$ **cp ~/admin-msp/signcerts/\* ~/admin-msp/admincerts/\***

### Configure your channel
1. Modify [configtx.yaml](templates/configtx.yaml) file attached on this workshop and copy paste it into your EC2 instance.

	>**REMEMBER TO REPLACE THE NAME AND ID WITH YOUR MEMBER ID**
	
	>**Important** :
	This file is sensitive. Artifacts from pasting can cause the file to fail with marshalling errors to **prevent the errors do the following commands** :
	

	<img src="images/tutorial-17.png" width="80%" style="margin-top: 10px;"/>
	
	COMMANDS :
	
	> **vi configtx.yaml** (Create new configtx.yaml file with vim editor)
	
	> **:set paste** (to prevent autoindenting)
	
	> **i** (below command will show INSERT (paste)
	
	> (Paste the file with cmd+v on Mac and right click on windows)
	
	> **esc**
	
	> **:set nopaste**
	
	> **:wq** (to write file and quit vim)
	
	
2. Generate the configtx channel configuration by executing the following script. 

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec cli configtxgen -outputCreateChannelTx /opt/home/$CHANNEL.pb -profile OneOrgChannel -channelID $CHANNEL --configPath /opt/home/**

	
	When the channel is created, this channel configuration will become the genesis block (i.e. block 0) on the channel. If it succeded you should see :
	<img src="images/tutorial-18.png" width="100%" style="margin-top: 10px"/>
	
3. Create the channel! 

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec -e "CORE\_PEER\_TLS\_ENABLED=true" -e "CORE\_PEER\_TLS\_ROOTCERT\_FILE=/opt/home/managedblockchain-tls-chain.pem" \\
    -e "CORE\_PEER\_ADDRESS=$PEER" -e "CORE\_PEER\_LOCALMSPID=$MSP" -e "CORE\_PEER\_MSPCONFIGPATH=$MSP\_PATH" \\
    cli peer channel create -c $CHANNEL -f /opt/home/$CHANNEL.pb -o $ORDERER --cafile $CAFILE --tls --timeout 900s**

	This will create a file called mychannel.block in the CLI container in the directory */opt/home/fabric-samples/chaincode/hyperledger/fabric/peer*.
	
### Join your peer node to the channel

1. Invite our peer node created on the network to this channel with the following command

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec -e "CORE\_PEER\_TLS\_ENABLED=true" -e "CORE\_PEER\_TLS\_ROOTCERT\_FILE=/opt/home/managedblockchain-tls-chain.pem" \\
    -e "CORE\_PEER\_ADDRESS=$PEER" -e "CORE\_PEER\_LOCALMSPID=$MSP" -e "CORE\_PEER\_MSPCONFIGPATH=$MSP\_PATH" \\
    cli peer channel join -b $CHANNEL.block  -o $ORDERER --cafile $CAFILE --tls**
    
### Install default chaincode into the channel

1. Install chaincode on Fabric peer with the following command

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec -e "CORE\_PEER\_TLS\_ENABLED=true" -e "CORE\_PEER\_TLS\_ROOTCERT\_FILE=/opt/home/managedblockchain-tls-chain.pem" \\
    -e "CORE\_PEER\_ADDRESS=$PEER" -e "CORE\_PEER\_LOCALMSPID=$MSP" -e "CORE\_PEER\_MSPCONFIGPATH=$MSP\_PATH" \\
    cli peer chaincode install -n $CHAINCODENAME -v $CHAINCODEVERSION -p $CHAINCODEDIR**

### Instantiate the chaincode on the channel
1. Instantiate the default chaincode on the channel with the following command

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec -e "CORE\_PEER\_TLS\_ENABLED=true" -e "CORE\_PEER\_TLS\_ROOTCERT\_FILE=/opt/home/managedblockchain-tls-chain.pem" \\
    -e "CORE\_PEER\_ADDRESS=$PEER" -e "CORE\_PEER\_LOCALMSPID=$MSP" -e "CORE\_PEER\_MSPCONFIGPATH=$MSP\_PATH" \\
    cli peer chaincode instantiate -o $ORDERER -C $CHANNEL -n $CHAINCODENAME -v $CHAINCODEVERSION \\
    -c '{"Args":["init","peer-a","100","peer-b","200"]}' --cafile $CAFILE --tls**

	This command will create 2 peers, peer-a with a wallet value 100 and peer-b with a wallet value of 200.
	
### Invoke a transaction
1. You can query the value in wallet of peer-a with this command

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec -e "CORE\_PEER\_TLS\_ENABLED=true" -e "CORE\_PEER\_TLS\_ROOTCERT\_FILE=/opt/home/managedblockchain-tls-chain.pem" \\
    -e "CORE\_PEER\_ADDRESS=$PEER" -e "CORE\_PEER\_LOCALMSPID=$MSP" -e "CORE\_PEER\_MSPCONFIGPATH=$MSP\_PATH" \\
    cli peer chaincode query -C $CHANNEL -n $CHAINCODENAME -c '{"Args":["query","peer-a"]}'**
    
2. Now Invoke a transaction with this command

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec -e "CORE\_PEER\_TLS\_ENABLED=true" -e "CORE\_PEER\_TLS\_ROOTCERT\_FILE=/opt/home/managedblockchain-tls-chain.pem" \\
    -e "CORE\_PEER\_ADDRESS=$PEER" -e "CORE\_PEER\_LOCALMSPID=$MSP" -e "CORE\_PEER\_MSPCONFIGPATH=$MSP\_PATH" \\
    cli peer chaincode invoke -o $ORDERER -C $CHANNEL -n $CHAINCODENAME \\
    -c '{"Args":["invoke","peer-a","peer-b","10"]}' --cafile $CAFILE --tls**

3. Query the chaincode again and check the change in value

	>[ec2-user@ip-10-0-176-173 ~]$ **docker exec -e "CORE\_PEER\_TLS\_ENABLED=true" -e "CORE\_PEER\_TLS\_ROOTCERT\_FILE=/opt/home/managedblockchain-tls-chain.pem" \\
    -e "CORE\_PEER\_ADDRESS=$PEER" -e "CORE\_PEER\_LOCALMSPID=$MSP" -e "CORE\_PEER\_MSPCONFIGPATH=$MSP\_PATH" \\
    cli peer chaincode query -C $CHANNEL -n $CHAINCODENAME -c '{"Args":["query","peer-a"]}'**

	(It should show "90" as 10 amount has been deducted from peer-a to peer-b)
	
## Conclusion

CONGRATULATIONS YOU HAVE LEARNED :

1. Creating a Hyperledger 1.2 Network with AWS Managed Blockchain Service
2. Connecting to your Hyperledger 1.2 Network thru Fabric Node Client inside an EC2 Instance

## Clean Up
To clean up your resources delete the Hyperledger Fabric network managed by Amazon Managed Blockchain and the AWS CloudFormation template as follows:

- In the AWS CloudFormation console **delete the stack with the stack name <your network>-fabric-client-node**

- In the Amazon Managed Blockchain console **delete the member for your network.** This will delete the peer node, the member, and finally, the Fabric network (assuming you created only one member)

## Reference
- [Blockchain Explained Simply](https://medium.com/swlh/blockchain-explained-simply-385dad966ce6)
- [Non-profit Blockchain Application with AWS MB](https://github.com/aws-samples/non-profit-blockchain/tree/master/ngo-fabric)
- [Hyperledger Fabric Build Your First Network](https://hyperledger-fabric.readthedocs.io/en/latest/build_network.html)
